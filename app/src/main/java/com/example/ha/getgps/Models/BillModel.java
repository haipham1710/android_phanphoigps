package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import tmt.common.datacontract.JsonHelper;
import tmt.common.datacontract.Parameter;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/13/16.
 */
public class BillModel implements Parcelable,Parameter{
    private String storeId;
    private String dateOrder;
    private double totalAmount;
    private String customerId;
    private String staffId;
    private String businessStaffId;
    private String note;
    private List<DetailModel> details;
    public BillModel()
    {

    }
    protected BillModel(Parcel in) {
        storeId = in.readString();
        dateOrder = in.readString();
        totalAmount = in.readDouble();
        customerId = in.readString();
        staffId = in.readString();
        businessStaffId = in.readString();
        note = in.readString();
        details = in.createTypedArrayList(DetailModel.CREATOR);
    }

    public static final BaseCreator<BillModel> CREATOR = new BaseCreator<BillModel>() {
        @Override
        public BillModel createFromParcel(Parcel in) {
            return new BillModel(in);
        }
        @Override
        public BillModel createFromJson(JsonHelper jsonHelper) {
            return new BillModel(jsonHelper);
        }

        @Override
        public BillModel[] newArray(int size) {
            return new BillModel[size];
        }
    };

    public BillModel(JsonHelper jsonHelper) {
        customerId = jsonHelper.readString("CustomerId");
    }

    public List<DetailModel> getDetails() {
        return details;
    }

    public void setDetails(List<DetailModel> details) {
        this.details = details;
    }

    public String getBusinessStaffId() {
        return businessStaffId;
    }

    public void setBusinessStaffId(String businessStaffId) {
        this.businessStaffId = businessStaffId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @Override
    public void toParameter(ParameterHelper parameterHelper) {
        parameterHelper.add("DateOrder",dateOrder)
                .add("TotalAmount",totalAmount)
                .add("CustomerId",customerId)
                .add("StaffId",staffId)
                .add("BusinessStaffId",businessStaffId)
                .add("Note",note)
                .add("Details",details);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(storeId);
        parcel.writeString(dateOrder);
        parcel.writeDouble(totalAmount);
        parcel.writeString(customerId);
        parcel.writeString(staffId);
        parcel.writeString(businessStaffId);
        parcel.writeString(note);
        parcel.writeTypedList(details);
    }
}
