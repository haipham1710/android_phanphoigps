package com.example.ha.getgps.Utils;


import android.content.Context;

import com.example.ha.getgps.Models.BillModel;
import com.example.ha.getgps.Models.CustomerModel;
import com.example.ha.getgps.Models.DiscountModel;
import com.example.ha.getgps.Models.IntervalTimeModel;
import com.example.ha.getgps.Models.LocationModel;
import com.example.ha.getgps.Models.LoginModel;
import com.example.ha.getgps.Models.ODataCustomerModel;
import com.example.ha.getgps.Models.ODataIntervalTimeModel;
import com.example.ha.getgps.Models.ODataProductModel;
import com.example.ha.getgps.Models.ODataStaffModel;
import com.example.ha.getgps.Models.ProductModel;
import com.example.ha.getgps.Models.StaffModel;

import java.net.HttpURLConnection;
import java.util.List;

import tmt.common.datacontract.Parameter;
import tmt.common.datacontract.ServiceBuilder2;
import tmt.common.net.Credentials;
import tmt.common.net.MethodType;
import tmt.common.threading.Task;
import tmt.common.threading.TaskContinuation;


/**
 * Created by tx on 8/18/16.
 */
public class Fetcher {
    private static final int Limit = 100;


    protected static ServiceBuilder2 with(String url)
    {
        return new ServiceBuilder2(Url.url+url);
    }
    public static Task<LoginModel> login(final String username, final String password)
    {
        Parameter parameter = new Parameter() {
            @Override
            public void toParameter(ParameterHelper parameterHelper) {
                parameterHelper.add("username",username)
                        .add("password",password)
                        .add("grant_type","password");
            }
        };
        return with(Url.token).setParameter(parameter)
                .method(MethodType.POST)
                .getResponseAsync(LoginModel.CREATOR);
    }

    //endregion
    public static Task<List<CustomerModel>> customers(Context context)
    {
        final UserPreferences userpre = new UserPreferences(context);
        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(HttpURLConnection httpURLConnection) {
                httpURLConnection.addRequestProperty("Authorization","Bearer "+userpre.getAccessToken());

            }
        };
        return with(Url.customer)
                .method(MethodType.GET)
                .authorize(credentials)
                .getResponseAsync(ODataCustomerModel.CREATOR)
                .then(new TaskContinuation<List<CustomerModel>, ODataCustomerModel>() {
            @Override
            public List<CustomerModel> onContinue(Task<ODataCustomerModel> task) throws Exception {
                return task.getResult().getCustomerModelList();
            }
        });
    }
    public static Task<List<StaffModel>> staffs(Context context)
    {
        final UserPreferences userpre = new UserPreferences(context);
        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(HttpURLConnection httpURLConnection) {
                httpURLConnection.addRequestProperty("Authorization","Bearer "+userpre.getAccessToken());

            }
        };
        return with(Url.staff)
                .method(MethodType.GET)
                .authorize(credentials)
                .getResponseAsync(ODataStaffModel.CREATOR)
                .then(new TaskContinuation<List<StaffModel>, ODataStaffModel>() {
            @Override
            public List<StaffModel> onContinue(Task<ODataStaffModel> task) throws Exception {
                return task.getResult().getStaffModelList();
            }
        });
    }
    public static Task<List<ProductModel>> products(Context context)
    {
        final UserPreferences userpre = new UserPreferences(context);
        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(HttpURLConnection httpURLConnection) {
                httpURLConnection.addRequestProperty("Authorization","Bearer "+userpre.getAccessToken());

            }
        };
        return with(Url.product)
                .method(MethodType.GET)
                .authorize(credentials)
                .getResponseAsync(ODataProductModel.CREATOR)
                .then(new TaskContinuation<List<ProductModel>, ODataProductModel>() {
            @Override
            public List<ProductModel> onContinue(Task<ODataProductModel> task) throws Exception {
                return task.getResult().getProductModelList();
            }
        });
    }
    public static Task<BillModel> insertBill(BillModel bill, Context context)
    {
        final UserPreferences userpre = new UserPreferences(context);

        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(HttpURLConnection httpURLConnection) {
                httpURLConnection.addRequestProperty("Authorization","Bearer "+userpre.getAccessToken());

            }
        };
        return with(Url.order).method(MethodType.POST).authorize(credentials)
                .setParameter(bill).getResponseAsync(BillModel.CREATOR);
    }
    public static Task<LocationModel> insertLocation(LocationModel locationModel,Context context)
    {
        final UserPreferences userpre = new UserPreferences(context);

        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(HttpURLConnection httpURLConnection) {
                httpURLConnection.addRequestProperty("Authorization","Bearer "+userpre.getAccessToken());

            }
        };

        return with(Url.location).setParameter(locationModel)
                .authorize(credentials)
                .method(MethodType.POST)
                .parameterEncodeType(ServiceBuilder2.ParameterEncodeType.JSON)
                .getResponseAsync(LocationModel.CREATOR);
    }
    public static Task<IntervalTimeModel> intervalTime()
    {
        return with(Url.interval).method(MethodType.GET).getResponseAsync(ODataIntervalTimeModel.CREATOR).then(new TaskContinuation<IntervalTimeModel, ODataIntervalTimeModel>() {
            @Override
            public IntervalTimeModel onContinue(Task<ODataIntervalTimeModel> task) throws Exception {
                if (task.isCancelled()) {
                    throw new Exception(task.getException());
                }
                if (task.isFaulted()) {
                    throw new Exception(task.getException());
                }
                if (task.isCompleted() && task.getResult() != null) {
                    return task.getResult().getIntervalTimeModelArrayList().get(0);
                }
                return null;
            }
        });
    }
    public static Task<DiscountModel> discount(Context context,Parameter params)
    {
        final UserPreferences userpre = new UserPreferences(context);

        Credentials credentials = new Credentials() {
            @Override
            public void prepareRequest(HttpURLConnection httpURLConnection) {
                httpURLConnection.addRequestProperty("Authorization","Bearer "+userpre.getAccessToken());
            }
        };
        return with(Url.discount)
                .method(MethodType.GET)
                .authorize(credentials)
                .setParameter(params)
                .getResponseAsync(DiscountModel.CREATOR);
    }
}
