package com.example.ha.getgps.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.ha.getgps.Models.ProductModel;
import com.example.ha.getgps.R;
import com.example.ha.getgps.Utils.Fetcher;
import com.example.ha.getgps.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import tmt.common.designinteraction.presentation.BaseRecyclerAdapter3;
import tmt.common.threading.Task;


public class RecyclerAdapter_Product extends BaseRecyclerAdapter3<ProductModel> implements Filterable {
    private final ItemListener listener;
    public interface ItemListener {
        void onItemProductClick(ProductModel product);
        void onCompleteFetch();
    }

    public RecyclerAdapter_Product(Context context, ItemListener listener) {
        super(context);
        this.listener = listener;

    }
    private ArrayList<ProductModel> productOrigModels;

    @Override
    public Filter getFilter() {
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                addAll((List<ProductModel>)results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ProductModel> results = new ArrayList<ProductModel>();
                if(constraint=="" || constraint==null)
                    constraint="";
                if (productOrigModels == null)
                    productOrigModels = new ArrayList<ProductModel>();
                if (productOrigModels.size() > 0) {
                    for (ProductModel productModel : productOrigModels) {
                        if (Utils.ConvertString(productModel.getName().toLowerCase())
                                .contains(Utils.ConvertString(constraint.toString()))
                                || productModel.getCode().contains(constraint.toString()))
                            results.add(productModel);
                    }
                }
                oReturn.values = results;
                oReturn.count=results.size();
                return oReturn;
            }
        };
    }
    @NonNull
    @Override
    protected Task<List<ProductModel>> realizeItems(int page) {
        return Fetcher.products(getContext());
    }

    @Override
    public void onCompleteFetch()
    {
        if (productOrigModels == null)
        {
            productOrigModels = new ArrayList<ProductModel>();
        }
        productOrigModels.addAll(getItems());
        notifyDataSetChanged();
        listener.onCompleteFetch();

    }
    @Override
    public VH onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.row_product, viewGroup, false);
        return new VH(this, itemView);
    }

    private static final class VH extends TypedViewHolder<ProductModel> {

        TextView tvNameSP;
        TextView tvPriceSP;
        TextView tvCode;
        ImageButton bt_add;
        public VH(RecyclerAdapter_Product adapter, View itemView) {
            super(itemView);
            tvNameSP = (TextView)view.findViewById(R.id.tv_ProductName);
            tvPriceSP = (TextView)view.findViewById(R.id.tv_ProductPrice);
            tvCode = (TextView)view.findViewById(R.id.tv_CodeProduct) ;
            bt_add = (ImageButton)view.findViewById(R.id.bt_add);
            final ItemListener listener = adapter.listener;
            bt_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemProductClick(item());
                }
            });
        }

        @Override
        public void bind(int position) {
            try {
                ProductModel productModel = item();
                if (productModel != null) {
                    tvNameSP.setText(productModel.getName());
                    tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf(Math.round(productModel.getPrice()))));
                    if (tvCode!= null){
                        tvCode.setText(String.valueOf(productModel.getCode()));
                    }
                }
            } catch (Exception exc) {
            }
        }
    }
}
