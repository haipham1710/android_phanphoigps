package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.Parameter;

/**
 * Created by tx on 10/13/16.
 */
public class DetailModel implements Parcelable,Parameter{
    private String id;
    private String productId;
    private String productUnitId;
    private double price;
    private String unitId;
    private  double discount;
    private double quantity;
    private String productName;
    private String productCode;
    private String STT;
    private String productUnitCode;
    public  DetailModel()
    {

    }
    private DetailModel(Parcel in) {
        id = in.readString();
        productId = in.readString();
        productUnitId = in.readString();
        price = in.readDouble();
        discount = in.readDouble();
        unitId = in.readString();
        quantity = in.readDouble();
        productName = in.readString();
        productCode = in.readString();
        STT = in.readString();
        productUnitCode = in.readString();
    }

    public static final Creator<DetailModel> CREATOR = new Creator<DetailModel>() {
        @Override
        public DetailModel createFromParcel(Parcel in) {
            return new DetailModel(in);
        }

        @Override
        public DetailModel[] newArray(int size) {
            return new DetailModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(String productUnitId) {
        this.productUnitId = productUnitId;
    }
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSTT() {
        return STT;
    }

    public void setSTT(String STT) {
        this.STT = STT;
    }

    public String getProductUnitCode() {
        return productUnitCode;
    }

    public void setProductUnitCode(String productUnitCode) {
        this.productUnitCode = productUnitCode;
    }
    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public void toParameter(ParameterHelper parameterHelper) {
        parameterHelper.add("ProductId",productId)
                .add("Product_UnitId",productUnitId)
                .add("Price",price)
                .add("Quantity",quantity)
                .add("Discount",discount)
                .add("ProductName",productName)
                .add("ProductCode",productCode)
                .add("STT",STT)
                .add("Product_UnitCode",productUnitCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(productId);
        parcel.writeString(productUnitId);
        parcel.writeDouble(price);
        parcel.writeString(unitId);
        parcel.writeDouble(discount);
        parcel.writeDouble(quantity);
        parcel.writeString(productName);
        parcel.writeString(productCode);
        parcel.writeString(STT);
        parcel.writeString(productUnitCode);
    }
}
