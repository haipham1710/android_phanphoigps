package com.example.ha.getgps.interaction;

import java.util.Date;

public class MessageBoxInterface {
    public interface OnDateChanged {
        void onDateChanged(Date value);
    }

    public interface OnInput {
        void onInput(String content);
    }
}
