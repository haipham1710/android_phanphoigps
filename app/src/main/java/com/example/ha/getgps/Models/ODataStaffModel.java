package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/11/16.
 */
public class ODataStaffModel implements Parcelable{

    private String odatacontext;
    private List<StaffModel> staffModelList;


    protected ODataStaffModel(Parcel in) {
        odatacontext = in.readString();
    }

    public static final BaseCreator<ODataStaffModel> CREATOR = new BaseCreator<ODataStaffModel>() {
        @Override
        public ODataStaffModel createFromParcel(Parcel in) {
            return new ODataStaffModel(in);
        }
        @Override
        public ODataStaffModel createFromJson(JsonHelper jsonHelper)
        {
            return new ODataStaffModel(jsonHelper);
        }
        @Override
        public ODataStaffModel[] newArray(int size) {
            return new ODataStaffModel[size];
        }
    };

    public ODataStaffModel(JsonHelper jsonHelper) {
        odatacontext = jsonHelper.readString("@odata.context");
        staffModelList = jsonHelper.readArrayList(StaffModel.CREATOR,"value");
    }

    public List<StaffModel> getStaffModelList() {
        return staffModelList;
    }

    public void setStaffModelList(List<StaffModel> staffModelList) {
        this.staffModelList = staffModelList;
    }

    public String getOdatacontext() {
        return odatacontext;
    }

    public void setOdatacontext(String odatacontext) {
        this.odatacontext = odatacontext;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(odatacontext);
    }
}
