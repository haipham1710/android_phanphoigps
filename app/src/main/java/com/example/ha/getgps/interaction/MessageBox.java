package com.example.ha.getgps.interaction;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public class MessageBox {

    private MessageBox() {
    }

    public static AlertDialog show(Context context, CharSequence message) {
        return show(context, message, "");
    }

    public static AlertDialog show(Context context, CharSequence message, CharSequence title) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("OK", null)
                .show();
    }

    public static void show(Context context, CharSequence message, final Runnable onOK) {
        show(context, message, null, onOK);
    }

    public static void show(Context context, CharSequence message, CharSequence title, final Runnable onOK) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        onOK.run();
                    }
                })
                .setNegativeButton("OK", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

    public static AlertDialog ask(Context context, CharSequence message, CharSequence title, final Runnable onOK) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("No", null)
                .setPositiveButton("Yes", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onOK.run();
                        dialog.dismiss();
                    }

                })
                .show();
    }
    public static AlertDialog ask(Context context, CharSequence message, CharSequence title, final Runnable onOK,String negativeButton,String positiveButton) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(negativeButton, null)
                .setPositiveButton(positiveButton, new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onOK.run();
                        dialog.dismiss();
                    }

                })
                .show();
    }
    public static AlertDialog input(Context context, int inputType, CharSequence content, CharSequence title, final MessageBoxInterface.OnInput onOK) {
        final EditText input = new EditText(context);
        FrameLayout frame = new FrameLayout(context);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.setMargins(10, 10, 10, 10);
        input.setText(content);
        input.setLayoutParams(params);
        input.setInputType(inputType);
        frame.addView(input);
        if (content != null) {
            input.setSelection(content.length(), content.length());
        }
        AlertDialog alert = new AlertDialog.Builder(context)
                .setTitle(title)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("OK", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onOK.onInput(input.getText().toString());
                    }
                })
                .create();

        alert.setView(frame);
        alert.show();
        return alert;
    }

}
