package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/17/16.
 */
public class ODataIntervalTimeModel implements Parcelable{
    private ArrayList<IntervalTimeModel> intervalTimeModelArrayList;
    private ODataIntervalTimeModel(Parcel in) {
        intervalTimeModelArrayList = in.createTypedArrayList(IntervalTimeModel.CREATOR);
    }

    public static final BaseCreator<ODataIntervalTimeModel> CREATOR = new BaseCreator<ODataIntervalTimeModel>() {
        @Override
        public ODataIntervalTimeModel createFromParcel(Parcel in) {
            return new ODataIntervalTimeModel(in);
        }
        @Override
        public ODataIntervalTimeModel createFromJson(JsonHelper jsonHelper) {
            return new ODataIntervalTimeModel(jsonHelper);
        }

        @Override
        public ODataIntervalTimeModel[] newArray(int size) {
            return new ODataIntervalTimeModel[size];
        }
    };

    private ODataIntervalTimeModel(JsonHelper jsonHelper) {
        intervalTimeModelArrayList =jsonHelper.readArrayList(IntervalTimeModel.CREATOR,"value");
    }

    public ArrayList<IntervalTimeModel> getIntervalTimeModelArrayList() {
        return intervalTimeModelArrayList;
    }

    public void setIntervalTimeModelArrayList(ArrayList<IntervalTimeModel> intervalTimeModelArrayList) {
        this.intervalTimeModelArrayList = intervalTimeModelArrayList;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(intervalTimeModelArrayList);
    }
}
