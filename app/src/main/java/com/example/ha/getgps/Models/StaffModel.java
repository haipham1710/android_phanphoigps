package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/14/16.
 */
public class StaffModel implements Parcelable{
    private String id;
    private String code;
    private String name;

    protected StaffModel(Parcel in) {
        id = in.readString();
        code = in.readString();
        name = in.readString();
    }

    public static final BaseCreator<StaffModel> CREATOR = new BaseCreator<StaffModel>() {
        @Override
        public StaffModel createFromParcel(Parcel in) {
            return new StaffModel(in);
        }
        @Override
        public StaffModel createFromJson(JsonHelper jsonHelper) {
            return new StaffModel(jsonHelper);
        }

        @Override
        public StaffModel[] newArray(int size) {
            return new StaffModel[size];
        }
    };

    public StaffModel(JsonHelper jsonHelper) {
        id = jsonHelper.readString("Id");
        code = jsonHelper.readString("Code");
        name = jsonHelper.readString("Name");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(code);
        parcel.writeString(name);
    }
}
