package com.example.ha.getgps.Utils;

import java.io.Serializable;

/**
 * Created by KP on 28/05/2016.
 */
public class UserModel implements Serializable {
    String UserName;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getAcess_Token() {
        return Acess_Token;
    }

    public void setAcess_Token(String acess_Token) {
        Acess_Token = acess_Token;
    }

    public String getToken_type() {
        return Token_type;
    }

    public void setToken_type(String token_type) {
        Token_type = token_type;
    }

    String Password;
    String Acess_Token;
    String Token_type;

}
