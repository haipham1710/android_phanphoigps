package com.example.ha.getgps.Utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserPreferences {
    private static final String KEY_USER_NAME = "username";
    private static final String KEY_PASS_WORD = "password";
    private static final String KEY_ACCESS_TOKEN = "access_token";
    private static final String KEY_CHECK = "checked";
    private static final String KEY_INTERVAL_TIME = "intervalInMinute";
    private final SharedPreferences prefs;

    public UserPreferences(Context application) {
        prefs = application.getSharedPreferences("com.example.ha.getgps", Context.MODE_PRIVATE);
    }

    public String getPassWord() {
        return prefs.getString(KEY_PASS_WORD, null);
    }

    public void setPassWord(String value) {
        prefs.edit().putString(KEY_PASS_WORD, value).apply();
    }


    public String getUserName() {
        return prefs.getString(KEY_USER_NAME, null);
    }

    public void setUserName(String value) {
        prefs.edit().putString(KEY_USER_NAME, value).apply();
        
    }


    public String getAccessToken() {
        return prefs.getString(KEY_ACCESS_TOKEN, null);
    }

    public void setAccessToken(String value) {
        prefs.edit().putString(KEY_ACCESS_TOKEN, value).apply();

    }

    public int getIntervalTime() {
        return prefs.getInt(KEY_INTERVAL_TIME, 60);
    }

    public void setIntervalTime(int value) {
        prefs.edit().putInt(KEY_INTERVAL_TIME, value).apply();

    }

    public boolean getChecked() {
        return prefs.getBoolean(KEY_CHECK, false);
    }

    public void setChecked(boolean value) {
        prefs.edit().putBoolean(KEY_CHECK, value).apply();
    }
    
    public void DeleteData() {
        prefs.edit().clear().apply();
    }
}
