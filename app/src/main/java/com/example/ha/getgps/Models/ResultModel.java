package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 9/12/16.
 */
public class ResultModel implements Parcelable{
    public boolean success;
    public String message;

    protected ResultModel(Parcel in) {
        success = in.readByte() != 0;
        message = in.readString();
    }

    public static final BaseCreator<ResultModel> CREATOR = new BaseCreator<ResultModel>() {
        @Override
        public ResultModel createFromParcel(Parcel in) {
            return new ResultModel(in);
        }
        @Override
        public ResultModel createFromJson(JsonHelper jsonHelper)
        {
            return new ResultModel(jsonHelper);
        }
        @Override
        public ResultModel[] newArray(int size) {
            return new ResultModel[size];
        }
    };

    public ResultModel(JsonHelper jsonHelper) {
        success=jsonHelper.readBoolean("success");
        message=jsonHelper.readString("message");
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeString(message);
    }
}
