package com.example.ha.getgps;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.WakefulBroadcastReceiver;

// make sure we use a WakefulBroadcastReceiver so that we acquire a partial wakelock
public class GpsTrackerAlarmReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = "GpsTrackerAlarmReceiver";
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        if(!CheckEnableGPSBuildNotification())
            return;
        else
        context.startService(new Intent(context, LocationService.class));
    }
    public boolean CheckEnableGPSBuildNotification()
    {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE );
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            buildNotiNoGPS();
            return false;
        }
        return true;
    }
    public void buildNotiNoGPS()
    {
        Intent intent = new Intent(context,Activity_Login.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intent, 0);
        NotificationManager manager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(true);
        builder.setTicker(context.getResources().getString(R.string.gps_unavailable));
        builder.setContentTitle(context.getResources().getString(R.string.gps_unavailable));
        builder.setContentText(context.getResources().getString(R.string.enable_gps));
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentIntent(pendingIntent);
        builder.setOngoing(true);
        builder.setSound(defaultSoundUri);
        builder.setSubText(context.getResources().getString(R.string.gps_service));   //API level 16
        builder.build();
        Notification myNotication = builder.build();
        manager.notify(11, myNotication);
    }
}
