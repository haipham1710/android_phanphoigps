package com.example.ha.getgps;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;

import com.example.ha.getgps.Adapter.AutoCompleteAdapter_Customer;
import com.example.ha.getgps.Adapter.AutoCompleteAdapter_Staff;
import com.example.ha.getgps.Models.CustomerModel;
import com.example.ha.getgps.Models.StaffModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class Fragment_Content extends Fragment {
    AutoCompleteTextView actv_customer;
    AutoCompleteTextView actv_staff;
    ImageButton bt_chooseDate;
    ImageButton bt_choosenTime;
    EditText et_dateOrder;
    EditText et_note;
    AutoCompleteAdapter_Customer adapterCustomer;
    AutoCompleteAdapter_Staff adapterStaff;
    Listener listener;
    String myFormat = "dd/MM/yyyy HH:mm";
    Calendar calendar;
    final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    public interface Listener{
        void onCustomerChange(CustomerModel customer);
        void onStaffChange(StaffModel staff);
        void onDateChange(String date);
        void onNoteChange(String note);
    }
    public void setListener(Listener listener)
    {
        this.listener = listener;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill_detail, container, false);
        actv_customer = (AutoCompleteTextView)view.findViewById(R.id.actv_customer);
        actv_staff = (AutoCompleteTextView)view.findViewById(R.id.actv_businessStaff);
        et_dateOrder = (EditText)view.findViewById(R.id.et_dateOrder);
        et_note = (EditText)view.findViewById(R.id.et_note);
        bt_chooseDate = (ImageButton)view.findViewById(R.id.bt_chooseDate);
        bt_choosenTime  = (ImageButton)view.findViewById(R.id.bt_chooseTime);
        adapterCustomer = new AutoCompleteAdapter_Customer(getActivity());
        adapterStaff = new AutoCompleteAdapter_Staff(getActivity());
        adapterStaff.fetch();
        adapterCustomer.fetch();
        actv_customer.setAdapter(adapterCustomer);
        actv_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actv_customer.showDropDown();
            }
        });
        actv_customer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                actv_customer.setText(adapterCustomer.getItem(position).getName());
                listener.onCustomerChange(adapterCustomer.getItem(position));
            }
        });

        actv_staff.setAdapter(adapterStaff);
        actv_staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actv_staff.showDropDown();
            }
        });
        actv_staff.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                actv_staff.setText(adapterStaff.getItem(position).getName());
                listener.onStaffChange(adapterStaff.getItem(position));
            }
        });
        calendar = Calendar.getInstance();
        restoreState();


        bt_chooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog dpd = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        calendar.set(Calendar.YEAR,year);
                        calendar.set(Calendar.MONTH,monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);


                        et_dateOrder.setText(sdf.format(calendar.getTime()));
                        listener.onDateChange(sdf.format(calendar.getTime()));

                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                dpd.show();
            }
        });
        bt_choosenTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TimePickerDialog dpd = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        calendar.set(Calendar.HOUR_OF_DAY,hour);
                        calendar.set(Calendar.MINUTE,minute);


                        et_dateOrder.setText(sdf.format(calendar.getTime()));
                        listener.onDateChange(sdf.format(calendar.getTime()));
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),false);
                dpd.show();
            }
        });
        et_note.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {
                listener.onNoteChange(editable.toString());
            }
        });
        return view;
    }
    public void restoreState()
    {
        if(getArguments()!=null) {
            //Get date and time to set to datetime picker
            if (getArguments().getString("dateOrder") != null) {
                et_dateOrder.setText(getArguments().getString("dateOrder"));
                Date date = null;
                try {
                    date = sdf.parse(getArguments().getString("dateOrder"));
                } catch (ParseException e) {
                    date = Calendar.getInstance().getTime();
                }
                calendar.setTime(date);

            }
            else
            {
                et_dateOrder.setText(sdf.format(calendar.getTime()));
                listener.onDateChange(sdf.format(calendar.getTime()));
            }
            if (getArguments().getString("noteOrder") != null)
                et_note.setText(getArguments().getString("noteOrder"));
            if (getArguments().getString("staffOrder") != null)
                actv_staff.setText(getArguments().getString("staffOrder"));
            if (getArguments().getString("customerOrder") != null)
                actv_customer.setText(getArguments().getString("customerOrder"));
        }
        else
        {
            et_dateOrder.setText(sdf.format(calendar.getTime()));
            listener.onDateChange(sdf.format(calendar.getTime()));
        }

    }
    public void clearData()
    {
        if(getArguments()!=null)
            getArguments().clear();
        if(et_dateOrder!=null) {
            et_dateOrder.setText(sdf.format(Calendar.getInstance().getTime()));
            listener.onDateChange(sdf.format(calendar.getTime()));
        }
        if(et_note!=null)
            et_note.setText("");
        if(actv_customer!=null)
            actv_customer.setText("");
        if(actv_staff!=null)
            actv_staff.setText("");
    }
}
