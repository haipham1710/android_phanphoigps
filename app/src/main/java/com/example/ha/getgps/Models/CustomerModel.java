package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/11/16.
 */
public class CustomerModel implements Parcelable{
    private String id;
    private String code;
    private String name;
    private String email;
    private String telephone;
    private String strAddress;

    protected CustomerModel(Parcel in) {
        id = in.readString();
        code = in.readString();
        name = in.readString();
        email = in.readString();
        telephone = in.readString();
        strAddress = in.readString();
    }

    public static final BaseCreator<CustomerModel> CREATOR = new BaseCreator<CustomerModel>() {
        @Override
        public CustomerModel createFromParcel(Parcel in) {
            return new CustomerModel(in);
        }
        @Override
        public CustomerModel createFromJson(JsonHelper json)
        {
            return new CustomerModel(json);
        }
        @Override
        public CustomerModel[] newArray(int size) {
            return new CustomerModel[size];
        }
    };

    public CustomerModel(JsonHelper json) {
        id = json.readString("Id","");
        code = json.readString("Code","");
        name = json.readString("Name","");
        email = json.readString("Email","");
        telephone = json.readString("Telephone","");
        strAddress = json.readString("StrAddress","");

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getStrAddress() {
        return strAddress;
    }

    public void setStrAddress(String strAddress) {
        this.strAddress = strAddress;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(code);
        parcel.writeString(name);
        parcel.writeString(email);
        parcel.writeString(telephone);
        parcel.writeString(strAddress);
    }
}
