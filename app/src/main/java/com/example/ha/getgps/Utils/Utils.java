package com.example.ha.getgps.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.animation.ScaleAnimation;

import com.example.ha.getgps.Models.LocationModel;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import tmt.common.threading.Task;
import tmt.common.threading.TaskCompletion;

/**
 * Created by KP on 03/05/2016.
 */
public class Utils {
    public static Context context;
    static DBHelper db;
    private static char[] charA = { 'à', 'á', 'ạ', 'ả', 'ã',// 0-4
            'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ',//5-10
            'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ' };//11-16
    private static char[] charE = { 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ',// 17-&gt;27
            'è', 'é', 'ẹ', 'ẻ', 'ẽ' };// e
    private static char[] charI = { 'ì', 'í', 'ị', 'ỉ', 'ĩ' };// i 28-&gt;32
    private static char[] charO = { 'ò', 'ó', 'ọ', 'ỏ', 'õ',// o 33-&gt;49
            'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ',// ô
            'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ' };// ơ
    private static char[] charU = { 'ù', 'ú', 'ụ', 'ủ', 'ũ',// u 50-&gt;60
            'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ' };// ư
    private static char[] charY = { 'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ' };// y 61-&gt;65
    private static char[] charD = { 'đ', ' ' }; // 66-67
    public static String makeRequest2(String method, String apiAddress, String accessToken, String mimeType, String requestBody) throws IOException {
        URL url = new URL(apiAddress);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(20000);
        urlConnection.setReadTimeout(20000);
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod(method);
        if(method.equals("POST") || method.equals("PUT")) {
            urlConnection.setDoOutput(!method.equals("GET"));
            urlConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
            urlConnection.setRequestProperty("Content-Type", mimeType);
            urlConnection.setRequestProperty("Accept", mimeType);
            OutputStream outputStream = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));
            writer.write(requestBody);
            writer.flush();
            writer.close();
            outputStream.close();
            urlConnection.connect();
        }

        InputStream inputStream;
        // get stream
        if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
            inputStream = urlConnection.getInputStream();

        } else {
            inputStream = urlConnection.getErrorStream();
        }
        // parse stream
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String temp, response = "";
        while ((temp = bufferedReader.readLine()) != null) {
            response += temp;
        }
        return response;
    }
    private static char GetAlterChar(char pC) {
        String charact = String.valueOf(charA, 0, charA.length)
                + String.valueOf(charE, 0, charE.length)
                + String.valueOf(charI, 0, charI.length)
                + String.valueOf(charO, 0, charO.length)
                + String.valueOf(charU, 0, charU.length)
                + String.valueOf(charY, 0, charY.length)
                + String.valueOf(charD, 0, charD.length);
        if ((int) pC == 32) {
            return ' ';
        }

        char tam = pC;// Character.toLowerCase(pC);

        int i = 0;
        while (i < charact.length() && charact.charAt(i) != tam) {
            i++;
        }
        if (i < 0 || i > 67)
            return pC;

        if (i == 66) {
            return 'd';
        }
        if (i >= 0 && i <= 16) {
            return 'a';
        }
        if (i >= 17 && i <= 27) {
            return 'e';
        }
        if (i >= 28 && i <= 32) {
            return 'i';
        }
        if (i >= 33 && i <= 49) {
            return 'o';
        }
        if (i >= 50 && i <= 60) {
            return 'u';
        }
        if (i >= 61 && i <= 65) {
            return 'y';
        }
        return pC;
    }
    public static String ConvertString(String pStr) {
        String convertString = pStr.toLowerCase();
        Character[] returnString = new Character[convertString.length()];
        for (int i = 0; i< convertString.length(); i++) {
            char temp = convertString.charAt(i);
            if ((int) temp < 97 || temp > 122) {
                char tam1 = GetAlterChar(temp);
                if ((int) temp != 32)
                    convertString = convertString.replace(temp, tam1);
            }
        }
        return convertString;
    }
    public static boolean CheckEnableGPSBuildAlert(){
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE );
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            buildAlertMessageNoGps();
            return false;
        }
        return true;

    }
    public static void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Yout GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    public synchronized static void Sync(Cursor resultSet,Context context)
    {
        db = new DBHelper(context);
        resultSet.moveToFirst();

        do {

            final Map<String, String> params = new HashMap<>();
            params.put("longitude", resultSet.getString(0));
            params.put("latitude", resultSet.getString(1));
            params.put("GPSTime",resultSet.getString(2));

            LocationModel locationModel = new LocationModel();
            locationModel.setDeviceTime(resultSet.getString(2));
            locationModel.setLat(resultSet.getString(1));
            locationModel.setLng(resultSet.getString(0));
            Fetcher.insertLocation(locationModel,context).then(new TaskCompletion<LocationModel>() {
                @Override
                public void onContinue(Task<LocationModel> task) throws Exception {
                    if (task.isCancelled()) {
                        Log.d("MESSAGE","errrorrrrrrr");

                        return;
                    }
                    if (task.isFaulted()) {

                        Log.d("MESSAGE",task.getException().getMessage());
                        return;
                    }
                    if (task.isCompleted() && task.getResult() != null) {
                        Log.d("MESSAGE",task.getResult().getDeviceTime());
                        db.deleteState(params.get("GPSTime"));
                    }
                }
            });
        } while (resultSet.moveToNext());



    }
    private static final char PARAMETER_DELIMITER = '&';
    private static final char PARAMETER_EQUALS_CHAR = '=';
    public static String createQueryStringForParameters(Map<String, String> parameters) throws UnsupportedEncodingException {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }

                parametersAsQueryString.append(parameterName)
                        .append(PARAMETER_EQUALS_CHAR)
                        .append(URLEncoder.encode(
                                parameters.get(parameterName),"UTF-8"));

                firstParameter = false;
            }
        }
        return parametersAsQueryString.toString();
    }
    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                //noinspection deprecation
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            Log.d("Network",
                                    "NETWORKNAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void ScaleAnimation(View v)
    {
        ScaleAnimation scale = new ScaleAnimation((float)1.0, (float)1.0, (float)0.0, (float)1.0);
        scale.setDuration(400);
        v.setAnimation(scale);
    }
    public static String ChangeDateFormat(String datetime)
    {
        try {
            SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = null;
            try {
                date = dateParser.parse(datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss");
            return dateFormatter.format(date);
        }
        catch(Exception e)
        {
            return "null";
        }
    }
    public static String ChangeDateFormat2(String datetime)
    {
        try {
            SimpleDateFormat dateParser = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            Date date = null;
            try {
                date = dateParser.parse(datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            return dateFormatter.format(date);
        }
        catch(Exception e)
        {
            return "null";
        }
    }
    public static String ConvertCurrency(String price)
    {
        String s="";
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        Boolean negative=false;
        if(price.substring(0,1).equals("-"))
            negative=true;
        price = price.replace("-","");

        s=formatter.format(Long.parseLong(price));
        if(negative)
            s="-"+s;

        return s.replace(",",".");
    }
}
