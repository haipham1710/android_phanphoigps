package com.example.ha.getgps.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ha.getgps.Models.DetailModel;
import com.example.ha.getgps.R;
import com.example.ha.getgps.Utils.Utils;

import tmt.common.designinteraction.presentation.BaseRecyclerAdapter3;


/**
 * Created by tx on 8/22/16.
 */
public class RecyclerAdaper_BillDetail extends BaseRecyclerAdapter3<DetailModel> {
    private final ItemListener listener;



    public interface ItemListener {
        void onBillDetailClick(DetailModel detail);
        void onQuantityChange();
    }

    public RecyclerAdaper_BillDetail(Context context, ItemListener listener) {
        super(context);
        this.listener = listener;

    }


    @Override
    public VH onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.row_product_chosen, viewGroup, false);
        return new VH(this, itemView);
    }

    /**
     * ViewHolder for item view of list
     */

    public static class VH extends TypedViewHolder<DetailModel> {
        TextView tvIndex;
        TextView tvName;
        TextView tvPrice;
        TextView tvDiscount;
        EditText etQuantity;
        TextView tvFinalPrice;
        public VH(final RecyclerAdaper_BillDetail adapter, View itemView) {
            super(itemView);
            tvIndex = (TextView)itemView.findViewById(R.id.tv_STT) ;
            tvName = (TextView)itemView.findViewById(R.id.tv_ProductName);
            tvPrice = (TextView)itemView.findViewById(R.id.tv_ProductPrice);
            tvDiscount = (TextView)itemView.findViewById(R.id.tv_discount);
            etQuantity = (EditText) itemView.findViewById(R.id.et_Quantity);
            tvFinalPrice = (TextView)itemView.findViewById(R.id.tv_FinalPricePerProduct);
            final ItemListener listener = adapter.listener;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onBillDetailClick(item());
                }
            });
            etQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(editable.toString().length()!=0 && !editable.toString().equals("."))
                        item().setQuantity(Double.parseDouble(editable.toString()));
                }
            });
            etQuantity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    adapter.notifyDataSetChanged();
                    listener.onQuantityChange();
                    return false;
                }
            });

        }

        @Override
        public void bind(int position) {
            super.bind(position);
            DetailModel detail = item();
            tvIndex.setText(String.valueOf(position+1));
            tvName.setText(detail.getProductName());
            tvPrice.setText(Utils.ConvertCurrency(String.valueOf(Math.round(detail.getPrice()))));
            tvDiscount.setText(detail.getDiscount()+"%");
            etQuantity.setText(String.valueOf(detail.getQuantity()));
            double total = detail.getQuantity()*detail.getPrice();
            double finalPrice = total - total * detail.getDiscount() / 100;
            tvFinalPrice.setText(Utils.ConvertCurrency(String.valueOf(Math.round(finalPrice))));

        }
    }
}

