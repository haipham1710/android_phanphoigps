package com.example.ha.getgps.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.File;

/**
 * Created by KP on 13/05/2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "location.sql";
    Context context;
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context=context;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onDelete(SQLiteDatabase db)
    {
        db.execSQL(SQL_DELETE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
    public Cursor getData()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+FeedEntry.TABLE_NAME, null );
        return res;
    }
    public boolean deleteState(String time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("Delete from Location where time='"+time+"'");
        Log.d("SQL:","Delete from Location where time='"+time+"'");
        //db.delete("LOCATION","time", new String[]{time});
        //db.delete("LocationModel",null,null);
        return true;
    }
    public boolean saveState(String longitude,String latitude,String time)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Longitude", longitude);
        contentValues.put("Latitude",latitude);
        contentValues.put("time",time);
        db.insert("Location", null, contentValues);
        return true;
    }
    public String getPath()
    {
        File path=context.getDatabasePath(DATABASE_NAME);
        return path.getAbsolutePath();
    }
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE IF NOT EXISTS " + FeedEntry.TABLE_NAME + "(Longitude VARCHAR,Latitude VARCHAR,time VARCHAR);";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "Location";
    }
}
