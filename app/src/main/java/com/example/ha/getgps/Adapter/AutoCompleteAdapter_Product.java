package com.example.ha.getgps.Adapter;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.ha.getgps.Models.ProductModel;
import com.example.ha.getgps.R;
import com.example.ha.getgps.Utils.Fetcher;
import com.example.ha.getgps.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import tmt.common.presentation.BaseListAdapter3;
import tmt.common.presentation.TypedViewHolder;
import tmt.common.presentation.ViewHolder;
import tmt.common.threading.Task;


public class AutoCompleteAdapter_Product extends BaseListAdapter3<ProductModel> implements Filterable {

    public AutoCompleteAdapter_Product(Context context) {
        super(context);
    }
    ArrayList<ProductModel> productOrigModels;

    @Override
    public Filter getFilter() {
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                addAll((List<ProductModel>)results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ProductModel> results = new ArrayList<ProductModel>();
                if(constraint=="" || constraint==null)
                    constraint="";
                if (productOrigModels == null)
                    productOrigModels = new ArrayList<ProductModel>();
                if (productOrigModels.size() > 0) {
                    for (ProductModel productModel : productOrigModels) {
                        if (Utils.ConvertString(productModel.getName().toLowerCase())
                                .contains(Utils.ConvertString(constraint.toString()))
                                || productModel.getCode().contains(constraint.toString()))
                            results.add(productModel);
                    }
                }
                oReturn.values = results;
                oReturn.count=results.size();
                return oReturn;
            }
        };
    }
    @Override
    protected Task<List<ProductModel>> realizeItems(int page) {
        return Fetcher.products(getContext());
    }
    @Override
    public void onCompleteFetch()
    {
        if (productOrigModels == null)
        {
            productOrigModels = new ArrayList<ProductModel>();
        }
        productOrigModels.addAll(getItems());

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VH(this, parent);
    }

    private static final class VH extends TypedViewHolder<ProductModel> {

        TextView tvNameSP;
        TextView tvPriceSP;
        TextView tvCode;

        public VH(AutoCompleteAdapter_Product prototype, ViewGroup parent) {
            super(prototype.inflater().inflate(R.layout.row_product, parent, false));
            tvNameSP = (TextView)view.findViewById(R.id.tv_ProductName);
            tvPriceSP = (TextView)view.findViewById(R.id.tv_ProductPrice);
            tvCode = (TextView)view.findViewById(R.id.tv_CodeProduct) ;
        }

        @Override
        public void bind(int position) {
            try {
                ProductModel productModel = item();
                if (productModel != null) {
                    tvNameSP.setText(productModel.getName());
                    tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf(Math.round(productModel.getPrice()))));
                    if (tvCode!= null){
                        tvCode.setText(String.valueOf(productModel.getCode()));
                    }
                }
            } catch (Exception exc) {
            }
        }
    }
}
