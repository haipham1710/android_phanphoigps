package com.example.ha.getgps.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.ha.getgps.Models.CustomerModel;
import com.example.ha.getgps.Models.StaffModel;
import com.example.ha.getgps.R;
import com.example.ha.getgps.Utils.Fetcher;
import com.example.ha.getgps.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import tmt.common.presentation.BaseListAdapter3;
import tmt.common.presentation.TypedViewHolder;
import tmt.common.presentation.ViewHolder;
import tmt.common.threading.Task;


public class AutoCompleteAdapter_Staff extends BaseListAdapter3<StaffModel> implements Filterable{

    public AutoCompleteAdapter_Staff(Context context) {
        super(context);
    }
    ArrayList<StaffModel> staffOrigModels;

    @Override
    public Filter getFilter() {
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                addAll((List<StaffModel>)results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<StaffModel> results = new ArrayList<StaffModel>();
                if(constraint=="" || constraint==null)
                    constraint="";
                if (staffOrigModels == null)
                    staffOrigModels = new ArrayList<>();
                if (staffOrigModels.size() > 0) {
                    for (StaffModel customerModel : staffOrigModels) {
                        if (Utils.ConvertString(customerModel.getName().toLowerCase())
                                .contains(Utils.ConvertString(constraint.toString())))
                            results.add(customerModel);
                    }
                }
                oReturn.values = results;
                oReturn.count=results.size();
                return oReturn;
            }
        };
    }
    @NonNull
    @Override
    protected Task<List<StaffModel>> realizeItems(int page) {
        return Fetcher.staffs(getContext());
    }
    @Override
    public void onCompleteFetch()
    {
        if (staffOrigModels == null) {
            staffOrigModels = new ArrayList<>();
        }
        staffOrigModels.addAll(getItems());

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VH(this, parent);
    }

    private static final class VH extends TypedViewHolder<StaffModel> {

        TextView tvCode;
        TextView tvAddress;

        public VH(AutoCompleteAdapter_Staff prototype, ViewGroup parent) {
            super(prototype.inflater().inflate(R.layout.row_customer_createbill, parent, false));
            tvCode = (TextView)view.findViewById(R.id.tv_CustomerCode);
            tvAddress = (TextView)view.findViewById(R.id.tv_CustomerAddress);
        }

        @Override
        public void bind(int position) {
            try {
                StaffModel staff = item();
                if (staff != null) {
                    String content =staff.getName();
                    tvCode.setText(content);
                    //tvAddress.setText(customer.getStrAddress());
                }
            } catch (Exception exc) {
            }
        }
    }
}
