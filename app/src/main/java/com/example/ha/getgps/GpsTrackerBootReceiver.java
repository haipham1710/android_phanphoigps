package com.example.ha.getgps;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;

import com.example.ha.getgps.Utils.UserPreferences;

public class GpsTrackerBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);

        SharedPreferences sharedPreferences = context.getSharedPreferences("com.example.ha.getgps", Context.MODE_PRIVATE);
        Boolean currentlyTracking = sharedPreferences.getBoolean("currentlyTracking", false);
        UserPreferences usefPref  = new UserPreferences(context);

        int intervalInMinutes = usefPref.getIntervalTime();
        if (usefPref.getAccessToken()!=null) {
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime(),
                    intervalInMinutes*1000,
                    pendingIntent);
        } else {
            alarmManager.cancel(pendingIntent);
        }
    }
}
