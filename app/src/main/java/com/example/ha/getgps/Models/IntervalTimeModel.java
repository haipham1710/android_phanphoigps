package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/17/16.
 */
public class IntervalTimeModel implements Parcelable{
    private int intervalInMinute;

    private IntervalTimeModel(Parcel in) {
        intervalInMinute = in.readInt();
    }

    public static final BaseCreator<IntervalTimeModel> CREATOR = new BaseCreator<IntervalTimeModel>() {
        @Override
        public IntervalTimeModel createFromParcel(Parcel in) {
            return new IntervalTimeModel(in);
        }
        @Override
        public IntervalTimeModel createFromJson(JsonHelper jsonHelper) {
            return new IntervalTimeModel(jsonHelper);
        }

        @Override
        public IntervalTimeModel[] newArray(int size) {
            return new IntervalTimeModel[size];
        }
    };

    private IntervalTimeModel(JsonHelper jsonHelper) {
        intervalInMinute = jsonHelper.readInteger("IntervalInMinute");
    }

    public int getIntervalInMinute() {
        return intervalInMinute;
    }

    public void setIntervalInMinute(int intervalInMinute) {
        this.intervalInMinute = intervalInMinute;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(intervalInMinute);
    }
}
