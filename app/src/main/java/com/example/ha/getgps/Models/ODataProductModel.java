package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/11/16.
 */
public class ODataProductModel implements Parcelable{

    private String odatacontext;
    private List<ProductModel> productModelList;

    protected ODataProductModel(Parcel in) {
        odatacontext = in.readString();
        productModelList = in.createTypedArrayList(ProductModel.CREATOR);
    }

    public static final BaseCreator<ODataProductModel> CREATOR = new BaseCreator<ODataProductModel>() {
        @Override
        public ODataProductModel createFromParcel(Parcel in) {
            return new ODataProductModel(in);
        }
        @Override
        public ODataProductModel createFromJson(JsonHelper jsonHelper)
        {
            return new ODataProductModel(jsonHelper);
        }
        @Override
        public ODataProductModel[] newArray(int size) {
            return new ODataProductModel[size];
        }
    };

    public ODataProductModel(JsonHelper jsonHelper) {
        odatacontext = jsonHelper.readString("@odata.context");
        productModelList = jsonHelper.readArrayList(ProductModel.CREATOR,"value");
    }

    public String getOdatacontext() {
        return odatacontext;
    }

    public void setOdatacontext(String odatacontext) {
        this.odatacontext = odatacontext;
    }

    public List<ProductModel> getProductModelList() {
        return productModelList;
    }

    public void setProductModelList(List<ProductModel> productModelList) {
        this.productModelList = productModelList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(odatacontext);
        parcel.writeTypedList(productModelList);
    }
}
