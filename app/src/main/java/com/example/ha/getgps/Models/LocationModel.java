package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.datacontract.Parameter;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/15/16.
 */
public class LocationModel implements Parcelable,Parameter{
    public String deviceTime;
    public String lng;

    public String lat;


    protected LocationModel(Parcel in) {
        deviceTime = in.readString();
    }
    public LocationModel()
    {

    }
    public static final BaseCreator<LocationModel> CREATOR = new BaseCreator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }
        @Override
        public LocationModel createFromJson(JsonHelper jsonHelper) {
            return new LocationModel(jsonHelper);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };

    public LocationModel(JsonHelper jsonHelper) {
        deviceTime = jsonHelper.readString("DeviceTime");
        lng = jsonHelper.readString("Lng");
        lat = jsonHelper.readString("Lat");
    }

    public String getDeviceTime() {
        return deviceTime;
    }

    public void setDeviceTime(String deviceTime) {
        this.deviceTime = deviceTime;
    }
    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(deviceTime);
    }

    @Override
    public void toParameter(ParameterHelper parameterHelper) {
        parameterHelper.add("Lng", lng)
                .add("Lat", lat)
                .add("DeviceTime",deviceTime);
    }
}
