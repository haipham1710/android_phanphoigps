package com.example.ha.getgps;

/**
 * Created by KP on 26/05/2016.
 */

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.ha.getgps.Models.LocationModel;
import com.example.ha.getgps.Utils.DBHelper;
import com.example.ha.getgps.Utils.Fetcher;
import com.example.ha.getgps.Utils.Utils;
import com.example.ha.getgps.interaction.MessageBox;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import tmt.common.datacontract.Parameter;
import tmt.common.threading.Task;
import tmt.common.threading.TaskCompletion;

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    DBHelper db;
    private GoogleApiClient mLocationClient;
    private boolean currentlyProcessingLocation = false;
    LocationRequest mLocationRequest;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO do something useful
        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            startTracking();
        }
        return Service.START_NOT_STICKY;
    }
    private void startTracking() {

        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {

            Toast.makeText(this, "Chạy dịch vụ GPS.", Toast.LENGTH_SHORT).show();
            mLocationClient = new GoogleApiClient.Builder(LocationService.this)
                    .addApi(LocationServices.API).addConnectionCallbacks(LocationService.this)
                    .addOnConnectionFailedListener(LocationService.this).build();
            if (!mLocationClient.isConnected() || !mLocationClient.isConnecting()) {
                mLocationClient.connect();
            }


        } else {
            Log.e("TAG", "unable to connect to google play services.");
        }
    }

    @Override
    public void onLocationChanged(final Location location) {
        // TODO Auto-generated method stub

        db = new DBHelper(getApplication());
        //Log.d("LOCATION:", location.getLongitude() + ", " + location.getLatitude());
        stopLocationUpdates();
        final String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).format(new Date());
        //Log.d("JSON:",date);
        Utils.context=this;
        if(!Utils.isConnectingToInternet(this)) {
            db.saveState(location.getLongitude()+"", location.getLatitude()+"", date);
            Log.d("Location not sync yet", String.valueOf(db.getData().getCount()));

        }
        else
        {
            if(db.getData().getCount()!=0) {
                Utils.Sync(db.getData(),this);
            }
            LocationModel locationModel = new LocationModel();
            locationModel.setDeviceTime(date);
            locationModel.setLat(String.valueOf(location.getLatitude()));
            locationModel.setLng(String.valueOf(location.getLongitude()));
            Fetcher.insertLocation(locationModel,this).then(new TaskCompletion<LocationModel>() {
                @Override
                public void onContinue(Task<LocationModel> task) throws Exception {
                    if (task.isCancelled()) {
                        Log.d("MESSAGE",getResources().getString(R.string.requiredServer));
                        db.saveState(String.valueOf(location.getLongitude()), String.valueOf(location.getLatitude()), date);

                        return;
                    }
                    if (task.isFaulted()) {
                        Log.d("MESSAGE",task.getException().getMessage());
                        db.saveState(String.valueOf(location.getLongitude()), String.valueOf(location.getLatitude()), date);

                        return;
                    }
                    if (task.isCompleted() && task.getResult() != null) {
                        Log.d("MESSAGE",task.getResult().getDeviceTime());
                    }
                }
            });
            db.close();

        }
        //Toast.makeText(this, location.getLongitude() + ", " + location.getLatitude(), Toast.LENGTH_SHORT).show();
        stopSelf();
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult arg0) {
        // TODO Auto-generated method stub
        stopLocationUpdates();
        stopSelf();
        Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(Bundle arg0) {
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        //}

    }
    private void stopLocationUpdates() {
        if (mLocationClient != null && mLocationClient.isConnected()) {
            mLocationClient.disconnect();
        }
    }
    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }



    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationClient.disconnect();

        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }
}
