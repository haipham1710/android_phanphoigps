package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/11/16.
 */
public class ODataCustomerModel implements Parcelable{

    private String odatacontext;
    private List<CustomerModel> customerModelList;

    private ODataCustomerModel(Parcel in) {
        odatacontext = in.readString();
        customerModelList = in.createTypedArrayList(CustomerModel.CREATOR);
    }

    public static final BaseCreator<ODataCustomerModel> CREATOR = new BaseCreator<ODataCustomerModel>() {
        @Override
        public ODataCustomerModel createFromParcel(Parcel in) {
            return new ODataCustomerModel(in);
        }
        @Override
        public ODataCustomerModel createFromJson(JsonHelper jsonHelper)
        {
            return new ODataCustomerModel(jsonHelper);
        }
        @Override
        public ODataCustomerModel[] newArray(int size) {
            return new ODataCustomerModel[size];
        }
    };

    private ODataCustomerModel(JsonHelper jsonHelper) {
        odatacontext = jsonHelper.readString("@odata.context");
        customerModelList = jsonHelper.readArrayList(CustomerModel.CREATOR,"value");
    }

    public String getOdatacontext() {
        return odatacontext;
    }

    public void setOdatacontext(String odatacontext) {
        this.odatacontext = odatacontext;
    }

    public List<CustomerModel> getCustomerModelList() {
        return customerModelList;
    }

    public void setCustomerModelList(List<CustomerModel> customerModelList) {
        this.customerModelList = customerModelList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(odatacontext);
        parcel.writeTypedList(customerModelList);
    }
}
