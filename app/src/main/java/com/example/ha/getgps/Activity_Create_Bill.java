package com.example.ha.getgps;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ha.getgps.Adapter.RecyclerAdaper_BillDetail;
import com.example.ha.getgps.Adapter.RecyclerAdapter_Product;
import com.example.ha.getgps.Models.BillModel;
import com.example.ha.getgps.Models.CustomerModel;
import com.example.ha.getgps.Models.DetailModel;
import com.example.ha.getgps.Models.DiscountModel;
import com.example.ha.getgps.Models.IntervalTimeModel;
import com.example.ha.getgps.Models.ProductModel;
import com.example.ha.getgps.Models.StaffModel;
import com.example.ha.getgps.Utils.Fetcher;
import com.example.ha.getgps.Utils.UserPreferences;
import com.example.ha.getgps.Utils.Utils;
import com.example.ha.getgps.interaction.MessageBox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import tmt.common.datacontract.Parameter;
import tmt.common.presentation.FetchableAdapter;
import tmt.common.threading.Task;
import tmt.common.threading.TaskCompletion;

public class Activity_Create_Bill extends AppCompatActivity implements RecyclerAdapter_Product.ItemListener, RecyclerAdaper_BillDetail.ItemListener, Fragment_Content.Listener {
    RecyclerAdaper_BillDetail adaperBillDetail;
    RecyclerAdapter_Product adapterProduct;
    RecyclerView rc_product;
    RecyclerView rc_billDetail;
    TextView tv_totalAmount;
    TextView tv_totalQuantity;
    SearchView sv_product;
    FragmentManager fragmentManager;
    FragmentTransaction transaction;
    CustomerModel customerOrder;
    StaffModel staffOrder;
    String dateOrder;
    String noteOrder;
    Fragment_Content content;
    UserPreferences userpre;
    RadioGroup rb_group;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_create_bill2);
        setContentView(R.layout.activity_create_bill);
        rc_product = (RecyclerView)findViewById(R.id.rc_product);
        rc_billDetail = (RecyclerView)findViewById(R.id.rc_billDetail);
        adapterProduct = new RecyclerAdapter_Product(this,this);
        dialog = new ProgressDialog(Activity_Create_Bill.this);
        adapterProduct.setFetchingListener(new FetchableAdapter.FetchingListener() {
            @Override
            public void onBeginFetch() {
                dialog.setMessage(getResources().getString(R.string.please_wait));
                dialog.show();
            }
            @Override
            public void onCompleteFetch() {
            }
            @Override
            public void hasException(Exception e) {
                dialog.setMessage("Xảy ra lỗi: "+e.getMessage());
            }
        });
        adaperBillDetail = new RecyclerAdaper_BillDetail(this,this);
        tv_totalAmount = (TextView)findViewById(R.id.tv_totalAmount);
        tv_totalQuantity = (TextView)findViewById(R.id.tv_QuantityTotal);
        sv_product = (SearchView)findViewById(R.id.sv_product);
        rb_group = (RadioGroup)findViewById(R.id.rb_group);
        userpre = new UserPreferences(getApplication());

        fragmentManager = getFragmentManager();
        content = (Fragment_Content) fragmentManager.findFragmentByTag("fragment_content");
        if(content==null) {
            content = new Fragment_Content();
            ((RadioButton)findViewById(R.id.rb1)).setChecked(true);
        }
        content.setListener(this);
        RadioGroup rb_group = (RadioGroup)findViewById(R.id.rb_group) ;
        rb_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                transaction = fragmentManager.beginTransaction();
                Log.d("selected index",String.valueOf(group.indexOfChild(findViewById(group.getCheckedRadioButtonId()))));
                if(group.indexOfChild(findViewById(group.getCheckedRadioButtonId()))==1)
                {
                    //shit is here, dont know why
                    if(!content.isAdded() && dateOrder!=null)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("dateOrder",dateOrder);
                        content.setArguments(bundle);
                    }
                    transaction.replace(R.id.ll_content,content,"fragment_content").commit();
                }
                else
                {
                    transaction.remove(content).commit();
                }
            }
        });
//        sw_content.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean attach) {
//                transaction = fragmentManager.beginTransaction();
//
//                if(attach)
//                {
//                    if(dateOrder!=null) //content.getArgument==null
//                    {
//                        Bundle bundle = new Bundle();
//                        bundle.putString("dateOrder",dateOrder);
//                        content.setArguments(bundle);
//                    }
//                    transaction.replace(R.id.ll_content,content,"fragment_content").commit();
//                }
//                else
//                {
//                    transaction.remove(content).commit();
//                }
//            }
//        });
        sv_product.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            android.widget.Filter filter = adapterProduct.getFilter();

            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                filter.filter(s);
                return false;
            }
        });

        LinearLayoutManager layoutManagerProduct = new LinearLayoutManager(this);
        LinearLayoutManager layoutManagerBillDetail = new LinearLayoutManager(this);
        rc_product.setLayoutManager(layoutManagerProduct);
        rc_product.setAdapter(adapterProduct);
        rc_billDetail.setLayoutManager(layoutManagerBillDetail);
        rc_billDetail.setAdapter(adaperBillDetail);
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                //Remove swiped item from list and notify the RecyclerView
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Activity_Create_Bill.this);
                builder.setTitle("Xoá "+adaperBillDetail.getItem(viewHolder.getAdapterPosition()).getProductName())
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                adaperBillDetail.remove(viewHolder.getAdapterPosition());
                                //adapterEmployeeChosen.remove(viewHolder.getAdapterPosition());
                                //adapterEmployeeChosen.notifyDataSetChanged();
                                adaperBillDetail.notifyDataSetChanged();
                                setSumPrice();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                adaperBillDetail.notifyDataSetChanged();
                                setSumPrice();
                            }
                        }).create().show();


            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rc_billDetail);
        CheckConnection();
        //if(userpre.getAccessToken()!=null)
          //  startAlarmManager();
    }

    public void CheckConnection() {
        //Check network
        Runnable run = new Runnable() {
            @Override
            public void run() {
                CheckConnection();
            }
        };
        if (!Utils.isConnectingToInternet(this)) {
            MessageBox.ask(this, getResources().getString(R.string.requiredConnect), getResources().getString(R.string.notify), run);
        } else {
            adapterProduct.fetch();
        }
    }
    @Override
    public void onItemProductClick(final ProductModel product) {
        final DetailModel detailModel = new DetailModel();
        detailModel.setProductName(product.getName());
        detailModel.setQuantity(1);
        detailModel.setPrice(product.getPrice());
        detailModel.setProductCode(product.getCode());
        detailModel.setProductId(product.getId());
        detailModel.setProductUnitId(product.getProductUnitId());
        detailModel.setDiscount(0);

        final String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).format(new Date());
        Parameter params = new Parameter() {
            @Override
            public void toParameter(ParameterHelper parameterHelper) {
                parameterHelper.add("ProductId",product.getId()).add("Date",date);
            }
        };
        Fetcher.discount(this,params).then(new TaskCompletion<DiscountModel>() {
            @Override
            public void onContinue(Task<DiscountModel> task) throws Exception {
                if(isDestroyed())
                    return;
                if (task.isCancelled()) {
                    MessageBox.show(Activity_Create_Bill.this,getResources().getString(R.string.requiredServer));
                    return;
                }
                if (task.isFaulted()) {
                    MessageBox.show(Activity_Create_Bill.this,task.getException().getMessage());
                    return;
                }
                if (task.isCompleted() && task.getResult() != null) {
                    detailModel.setDiscount(task.getResult().getValue());
                    adaperBillDetail.notifyDataSetChanged();
                    setSumPrice();
                }
            }
        });

        //tvQuantityTotal.setText(Utils.ConvertCurrency(String.valueOf(quantityTotal())));
        boolean flag = false;
        for (DetailModel detail:adaperBillDetail.toList()) {
            if(detail.getProductName().equals(detailModel.getProductName())) {
                detail.setQuantity(detail.getQuantity() + 1);
                flag = true;
            }
        }
        if(!flag)
            adaperBillDetail.add(detailModel);
        adaperBillDetail.notifyDataSetChanged();
        setSumPrice();
    }

    @Override
    public void onCompleteFetch() {
        if(dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onBillDetailClick(DetailModel detail) {

    }

    @Override
    public void onQuantityChange() {
        setSumPrice();
    }

    public void setSumPrice(){
        long sumPrice = 0;
        double totalQuantity = 0;
        if (adaperBillDetail!= null)
        {
            for (DetailModel productModel: adaperBillDetail.toList() )
            {
                double dongia = 0;
                dongia = productModel.getPrice();
                sumPrice = Math.round(sumPrice +((productModel.getQuantity() * dongia) - (productModel.getQuantity() * dongia)*productModel.getDiscount()/100 )) ;
                totalQuantity += productModel.getQuantity();
            }

        }
        tv_totalAmount.setText(Utils.ConvertCurrency(String.valueOf(sumPrice)));
        tv_totalQuantity.setText(String.valueOf(totalQuantity));
    }

    @Override
    public void onCustomerChange(CustomerModel customer) {
        this.customerOrder = customer;
    }
    @Override
    public void onStaffChange(StaffModel staff)
    {
        this.staffOrder = staff;
        Toast.makeText(this,staffOrder.getId(),Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onDateChange(String date)
    {
        dateOrder=date;
    }
    @Override
    public void onNoteChange(String note)
    {
        noteOrder=note;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("dateOrder",dateOrder);
        savedInstanceState.putString("noteOrder",noteOrder);
        savedInstanceState.putParcelable("customerOrder",customerOrder);
        savedInstanceState.putParcelable("staffOrder",staffOrder);
        savedInstanceState.putParcelableArrayList("adapterBillDetail",adaperBillDetail.toList());
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onRestoreInstanceState(savedInstanceState);
        dateOrder = savedInstanceState.getString("dateOrder");
        noteOrder = savedInstanceState.getString("noteOrder");
        customerOrder = savedInstanceState.getParcelable("customerOrder");
        staffOrder = savedInstanceState.getParcelable("staffOrder");
        ArrayList<DetailModel> detailModelList= savedInstanceState.getParcelableArrayList("adapterBillDetail");
        if (detailModelList != null) {
            adaperBillDetail.addAll(detailModelList);
        }
        if(fragmentManager.findFragmentByTag("fragment_content")==null) {
            Bundle bundle = new Bundle();
            if(dateOrder!=null)
                bundle.putString("dateOrder",dateOrder);
            if(noteOrder!=null)
                bundle.putString("noteOrder",noteOrder);
            if(customerOrder!=null)
                bundle.putString("customerOrder",customerOrder.getName());
            if(staffOrder!=null)
                bundle.putString("staffOrder",staffOrder.getName());
            content.setArguments(bundle);
        }
        setSumPrice();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id==R.id.action_logout)
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Create_Bill.this);
            builder.setTitle(getResources().getString(R.string.request_logout))
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            UserPreferences userpre = new UserPreferences(getApplication());
                            userpre.DeleteData();
                            cancelAlarmManager();
                            Intent intent_Login = new Intent(Activity_Create_Bill.this,Activity_Login.class);
                            finish();
                            startActivity(intent_Login);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        if(id==R.id.action_refresh)
        {
            clearData();
        }
        if(id==R.id.action_save)
        {
            if(customerOrder==null)
            {
                MessageBox.show(Activity_Create_Bill.this,getResources().getString(R.string.missing_data_customer));
                return false;
            }
            if(staffOrder==null)
            {
                MessageBox.show(Activity_Create_Bill.this,getResources().getString(R.string.missing_data_staff));
                return false;
            }
            BillModel bill = new BillModel();
            bill.setCustomerId(customerOrder.getId());
            bill.setDateOrder(Utils.ChangeDateFormat2(dateOrder));
            bill.setTotalAmount(Double.parseDouble(tv_totalAmount.getText().toString().replace(".","")));
            bill.setBusinessStaffId(staffOrder.getId());
            bill.setDetails(adaperBillDetail.toList());
            bill.setNote(noteOrder);
            //Fetcher.application = getApplication();
            dialog.show();
            Fetcher.insertBill(bill,Activity_Create_Bill.this).then(new TaskCompletion<BillModel>() {
                @Override
                public void onContinue(Task<BillModel> task) throws Exception {
                    if(dialog.isShowing())
                        dialog.dismiss();
                    if (isDestroyed()) {
                        return;
                    }
                    if (task.isCancelled()) {
                        MessageBox.show(Activity_Create_Bill.this, getResources().getString(R.string.requiredServer), getResources().getString(R.string.notify));
                        return;
                    }
                    if (task.isFaulted()) {
                        MessageBox.show(Activity_Create_Bill.this, task.getException().getMessage(), getResources().getString(R.string.notify));
                        return;
                    }
                    if (task.isCompleted() && task.getResult() != null) {
                        MessageBox.show(Activity_Create_Bill.this,getResources().getString(R.string.create_bill_success));
                        //Toast.makeText(Activity_Create_Bill2.this,task.getResult().getCustomerId(),Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }
    public void clearData()
    {
        dateOrder = null;
        noteOrder = null;
        customerOrder= null;
        staffOrder= null;

        content = (Fragment_Content) fragmentManager.findFragmentByTag("fragment_content");
        if(content==null) {
            content = new Fragment_Content();
            content.setListener(this);
        }
        else
            content.clearData();

        adaperBillDetail.clear();
        adaperBillDetail.notifyDataSetChanged();
        setSumPrice();
    }

    private void startAlarmManager() {
        Fetcher.intervalTime().then(new TaskCompletion<IntervalTimeModel>() {
            @Override
            public void onContinue(Task<IntervalTimeModel> task) throws Exception {
                if(isDestroyed())
                    return;
                if (task.isCancelled()) {
                    MessageBox.show(Activity_Create_Bill.this,getResources().getString(R.string.requiredServer));
                    return;
                }
                if (task.isFaulted()) {
                    MessageBox.show(Activity_Create_Bill.this,task.getException().getMessage());
                    return;
                }
                if (task.isCompleted() && task.getResult() != null) {
                    Log.d("TAG", "startAlarmManager with interval = "+task.getResult().getIntervalInMinute());

                    Context context = getBaseContext();
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReceiver.class);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);
                    alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), task.getResult().getIntervalInMinute()*1000, pendingIntent);
                    userpre.setIntervalTime( task.getResult().getIntervalInMinute());

                }
            }
        });

    }
    @Override
    public void onBackPressed()
    {
        MessageBox.ask(this,getResources().getString(R.string.confirm_exit),"",new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }
            );
    }
    private void cancelAlarmManager() {
        Log.d("TAG", "cancelAlarmManager");

        Context context = getBaseContext();
        Intent gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);

    }
}
