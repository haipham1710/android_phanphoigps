package com.example.ha.getgps.Utils;

/**
 * Created by KP on 08/06/2016.
 */
public class ActionModel {
    public String getAction_name() {
        return action_name;
    }

    public void setAction_name(String action_name) {
        this.action_name = action_name;
    }

    public String getAction_detail() {
        return action_detail;
    }

    public void setAction_detail(String action_detail) {
        this.action_detail = action_detail;
    }

    String action_name;
    String action_detail;
}
