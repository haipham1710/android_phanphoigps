package com.example.ha.getgps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ha.getgps.Models.LoginModel;
import com.example.ha.getgps.Utils.Fetcher;
import com.example.ha.getgps.Utils.UserPreferences;
import com.example.ha.getgps.Utils.Utils;
import com.example.ha.getgps.interaction.MessageBox;

import tmt.common.threading.Task;
import tmt.common.threading.TaskCompletion;


public class Activity_Login extends AppCompatActivity {


    private EditText et_username;
    private EditText et_pass;
    private CheckBox cb_save;
    Button btn_Login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //Fetcher.application=getApplication();
        btn_Login = (Button) findViewById(R.id.btn_Login);
        et_username = (EditText) findViewById(R.id.et_Username);
        et_pass = (EditText) findViewById(R.id.et_Password);
        et_pass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    btn_Login.performClick();
                    return false;
                }
                return false;
            }
        });
        cb_save = (CheckBox) findViewById(R.id.cb_save);
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (et_username.getText().toString().length() == 0) {
                et_username.setError(getResources().getString(R.string.require_username));
                et_username.requestFocus();
            } else if(et_pass.getText().toString().length() == 0)
            {
                et_pass.setError(getResources().getString(R.string.require_password));
                et_pass.requestFocus();
            }
                else
                {
                    if (!Utils.isConnectingToInternet(Activity_Login.this)) {
                        MessageBox.show(Activity_Login.this,getResources().getString(R.string.requiredConnect),getResources().getString(R.string.notify));

                    } else {
                        final ProgressDialog dialog = new ProgressDialog(Activity_Login.this);
                        dialog.setMessage(getResources().getString(R.string.please_wait));
                        dialog.show();
                        //new LoginTask().execute(Url.url + Url.user);
                        Fetcher.login(et_username.getText().toString().trim(),et_pass.getText().toString().trim()).then(new TaskCompletion<LoginModel>() {
                            @Override
                            public void onContinue(Task<LoginModel> task) throws Exception {
                                if(dialog.isShowing())
                                    dialog.dismiss();
                                if (isDestroyed()) {
                                    return;
                                }
                                if (task.isCancelled()) {
                                    MessageBox.show(Activity_Login.this, getResources().getString(R.string.requiredServer), getResources().getString(R.string.notify));
                                    return;
                                }
                                if (task.isFaulted()) {
                                    MessageBox.show(Activity_Login.this, task.getException().getMessage(), getResources().getString(R.string.notify));
                                    return;
                                }
                                LoginModel loginModel = task.getResult();
                                if (loginModel!=null) {
                                    if(loginModel.getError_description().equals(""))
                                    {
                                        Intent intent_Login = new Intent(Activity_Login.this,Activity_Create_Bill.class);
                                        doSaveDataLogin(loginModel.getAccess_token());
                                        finish();
                                        startActivity(intent_Login);
                                    }
                                    else
                                        MessageBox.show(Activity_Login.this,loginModel.getError_description());
                                }
                            }
                        });
                    }
                }

            }
        });
        doGetDataLogin();

    }

    private void doSaveDataLogin(String access_token) {
        UserPreferences userPref = new UserPreferences(getApplication());

        String username = et_username.getText().toString();
        String password = et_pass.getText().toString();
        boolean isChecked = cb_save.isChecked();
        userPref.setUserName(username);
        userPref.setPassWord(password);
        userPref.setChecked(isChecked);
        userPref.setAccessToken(access_token);
    }



    private void doGetDataLogin() {

        UserPreferences userPref = new UserPreferences(getApplication());
        boolean isChecked = userPref.getChecked();
        if (isChecked) {
            et_username.setText(userPref.getUserName());
            et_pass.setText(userPref.getPassWord());
        }
        cb_save.setChecked(isChecked);
    }


}
