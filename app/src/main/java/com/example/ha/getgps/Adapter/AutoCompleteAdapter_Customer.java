package com.example.ha.getgps.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.ha.getgps.Models.CustomerModel;
import com.example.ha.getgps.R;
import com.example.ha.getgps.Utils.Fetcher;
import com.example.ha.getgps.Utils.Utils;

import java.util.ArrayList;
import java.util.List;

import tmt.common.presentation.BaseListAdapter3;
import tmt.common.presentation.TypedViewHolder;
import tmt.common.presentation.ViewHolder;
import tmt.common.threading.Task;


public class AutoCompleteAdapter_Customer extends BaseListAdapter3<CustomerModel> implements Filterable{

    public AutoCompleteAdapter_Customer(Context context) {
        super(context);
    }
    ArrayList<CustomerModel> customerOrigModels;

    @Override
    public Filter getFilter() {
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                addAll((List<CustomerModel>)results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<CustomerModel> results = new ArrayList<CustomerModel>();
                if(constraint=="" || constraint==null)
                    constraint="";
                if (customerOrigModels == null)
                    customerOrigModels = new ArrayList<>();
                if (customerOrigModels != null && customerOrigModels.size() > 0) {
                    for (CustomerModel customerModel : customerOrigModels) {
                        if (Utils.ConvertString(customerModel.getName().toLowerCase())
                                .contains(Utils.ConvertString(constraint.toString()))
                                || customerModel.getTelephone().contains(constraint.toString()))
                            results.add(customerModel);
                    }
                }
                oReturn.values = results;
                oReturn.count=results.size();
                return oReturn;
            }
        };
    }
    @NonNull
    @Override
    protected Task<List<CustomerModel>> realizeItems(int page) {
        return Fetcher.customers(getContext());
    }
    @Override
    public void onCompleteFetch()
    {
        if (customerOrigModels == null) {
            customerOrigModels = new ArrayList<>();
        }
        customerOrigModels.addAll(getItems());

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VH(this, parent);
    }

    private static final class VH extends TypedViewHolder<CustomerModel> {

        TextView tvCode;
        TextView tvAddress;

        public VH(AutoCompleteAdapter_Customer prototype, ViewGroup parent) {
            super(prototype.inflater().inflate(R.layout.row_customer_createbill, parent, false));
            tvCode = (TextView)view.findViewById(R.id.tv_CustomerCode);
            tvAddress = (TextView)view.findViewById(R.id.tv_CustomerAddress);
        }

        @Override
        public void bind(int position) {
            try {
                CustomerModel customer = item();
                if (customer != null) {
                    String content = String.valueOf(customer.getName()+" - "+customer.getTelephone());
                    tvCode.setText(content);
                    tvAddress.setText(customer.getStrAddress());
                }
            } catch (Exception exc) {
            }
        }
    }
}
