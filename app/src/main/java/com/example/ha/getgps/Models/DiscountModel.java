package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/24/16.
 */

public class DiscountModel implements Parcelable {


    private double value;

    private DiscountModel(Parcel in) {
        value = in.readDouble();
    }

    public static final BaseCreator<DiscountModel> CREATOR = new BaseCreator<DiscountModel>() {
        @Override
        public DiscountModel createFromParcel(Parcel in) {
            return new DiscountModel(in);
        }
        @Override
        public DiscountModel createFromJson(JsonHelper jsonHelper) {
            return new DiscountModel(jsonHelper);
        }

        @Override
        public DiscountModel[] newArray(int size) {
            return new DiscountModel[size];
        }
    };

    private DiscountModel(JsonHelper jsonHelper) {
        value = jsonHelper.readDouble("value");
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(value);
    }
}
