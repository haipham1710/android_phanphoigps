package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 10/11/16.
 */
public class ProductModel implements Parcelable{
    private String id;
    private String code;
    private String name;
    private String unitId;
    private double importedAmount;
    private double price;
    private double quantity;
    private String productUnitId;

    protected ProductModel(Parcel in) {
        id = in.readString();
        code = in.readString();
        name = in.readString();
        unitId = in.readString();
        importedAmount = in.readDouble();
        price = in.readDouble();
        quantity = in.readDouble();
        productUnitId = in.readString();
    }

    public static final BaseCreator<ProductModel> CREATOR = new BaseCreator<ProductModel>() {
        @Override
        public ProductModel createFromParcel(Parcel in) {
            return new ProductModel(in);
        }
        @Override
        public ProductModel createFromJson(JsonHelper jsonHelper)
        {
            return new ProductModel(jsonHelper);
        }
        @Override
        public ProductModel[] newArray(int size) {
            return new ProductModel[size];
        }
    };

    public ProductModel(JsonHelper jsonHelper) {
        id = jsonHelper.readString("Id");
        code = jsonHelper.readString("Code");
        name = jsonHelper.readString("Name");
        unitId = jsonHelper.readString("UnitId");
        importedAmount = jsonHelper.readDouble("ImportedAmount");
        price = jsonHelper.readDouble("Price");
        quantity = jsonHelper.readDouble("Quantity");
        productUnitId = jsonHelper.readString("Product_UnitId");

    }

    public ProductModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String productId) {
        this.id = productId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getImportedAmount() {
        return importedAmount;
    }

    public void setImportedAmount(double importedAmount) {
        this.importedAmount = importedAmount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(String productUnitId) {
        this.productUnitId = productUnitId;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(code);
        parcel.writeString(name);
        parcel.writeString(unitId);
        parcel.writeDouble(importedAmount);
        parcel.writeDouble(price);
        parcel.writeDouble(quantity);
        parcel.writeString(productUnitId);
    }
}
