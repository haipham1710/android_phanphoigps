package com.example.ha.getgps.Models;

import android.os.Parcel;
import android.os.Parcelable;

import tmt.common.datacontract.JsonHelper;
import tmt.common.util.BaseCreator;

/**
 * Created by tx on 8/29/16.
 */
public class LoginModel implements Parcelable{
    public String error_description;
    public String access_token;
    public String expires;



    protected LoginModel(Parcel in) {
        error_description = in.readString();
        access_token = in.readString();
        expires = in.readString();
    }

    public static final BaseCreator<LoginModel> CREATOR = new BaseCreator<LoginModel>() {
        @Override
        public LoginModel createFromParcel(Parcel in) {
            return new LoginModel(in);
        }
        public LoginModel createFromJson(JsonHelper jsonHelper)
        {
            return new LoginModel(jsonHelper);
        }
        @Override
        public LoginModel[] newArray(int size) {
            return new LoginModel[size];
        }
    };

    public LoginModel(JsonHelper jsonHelper) {
        this.error_description = jsonHelper.readString("error_description","");
        this.access_token = jsonHelper.readString("access_token","");
        this.expires = jsonHelper.readString("expires","");
    }


    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }
    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(error_description);
        parcel.writeString(access_token);
        parcel.writeString(expires);
    }
}
